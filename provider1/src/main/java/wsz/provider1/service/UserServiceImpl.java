package wsz.provider1.service;

import io.netty.channel.ChannelHandler;
import org.springframework.stereotype.Component;
import wsz.api.IUserService;
import wsz.pojo.User;
import wsz.rpc.provider.anno.RpcService;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author wsz
 * @date 2021/12/1 16:45
 **/
@Component
@RpcService
@ChannelHandler.Sharable
public class UserServiceImpl implements IUserService {

    Map<Integer, User> userMap = new HashMap<>();

    @PostConstruct
    public void intUsers() {
        userMap.put(1, new User(1, "provider1：1"));
        userMap.put(2, new User(2, "provider1：2"));
        userMap.put(3, new User(3, "provider1：3"));
        userMap.put(4, new User(4, "provider1：4"));
    }
    private Random random = new Random();

    @Override
    public User getById(int id) {
        try {
            if (id % 4 == 0) {
                int nextInt = random.nextInt(1000) * 1;
                System.out.println("provider1-sleep:" + nextInt);
                Thread.sleep(nextInt);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return userMap.get(id);
    }
}

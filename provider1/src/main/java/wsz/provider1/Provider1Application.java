package wsz.provider1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import wsz.api.IUserService;
import wsz.rpc.provider.server.RpcServer;

@ComponentScans(value = {@ComponentScan("wsz.rpc.provider")})
@SpringBootApplication
public class Provider1Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Provider1Application.class, args);
    }

    @Autowired
    private RpcServer rpcServer;

    @Value("${netty.host}")
    private String nettyHost;

    @Value("${netty.port}")
    private int nettyPort;

    @Override
    public void run(String... args) throws Exception {
        new Thread(() -> {
            rpcServer.startServer(nettyHost, nettyPort);
        }).start();
    }
}

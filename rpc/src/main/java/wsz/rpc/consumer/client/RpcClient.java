package wsz.rpc.consumer.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.util.Objects;
import java.util.concurrent.*;

/**
 * @author wsz
 * @date 2021/11/30 18:16
 **/
public class RpcClient {

    private EventLoopGroup group;

    private Channel channel;

    private String ip;

    private int port;
    //
    private long time;

    public void setTime(long time) {
        this.time = time;
    }

    public long getTime() {
        return time;
    }

    private RpcClientHandler rpcClientHandler = new RpcClientHandler();

    private ExecutorService executorService = new ThreadPoolExecutor(
            10,
            20,
            3000,
            TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(100)
    );

    public RpcClient(String ip, int port) {
        this.ip = ip;
        this.port = port;
        this.time = 0;
        initClient();
    }
    public String getConnect() {
        return ip + ":" +port;
    }

    public void initClient() {
        try {
            group = new NioEventLoopGroup();
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, Boolean.TRUE)
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 3000)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast(new StringDecoder());
                            pipeline.addLast(new StringEncoder());
                            pipeline.addLast(rpcClientHandler);
                        }
                    });
            channel = bootstrap.connect(ip, port).sync().channel();
            System.out.println("----客户端启动成功----" + ip + ":" + port);
        } catch (Exception ex) {
            ex.printStackTrace();
            close();
        }
    }

    public void close() {
        if (channel != null) {
            channel.close();
        }
        if (group != null) {
            group.shutdownGracefully();
        }
    }

    public Object send(String msg) throws Exception {
        rpcClientHandler.setRequestMsg(msg);
        Future submit = executorService.submit(rpcClientHandler);
        return submit.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RpcClient rpcClient = (RpcClient) o;
        return port == rpcClient.port && ip.equals(rpcClient.ip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, port);
    }
}

package wsz.rpc.common;

/**
 * 常量
 * @author wsz
 * @date 2021/12/7 18:20
 **/
public class ConstantsUtils {
    public static final String PARENT_NODE = "/wsz";
    public static final String NODE_DATA_HANDLE = "handle";
    public static final String NODE_DATA_TIME = "time";
}

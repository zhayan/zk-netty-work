package wsz.consumer.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import wsz.api.IUserService;
import wsz.rpc.consumer.proxy.RpcClientProxy;

import javax.annotation.PostConstruct;

/**
 * @author wsz
 * @date 2021/12/1 17:46
 **/
@RestController
public class UserController {

    @Autowired
    private RpcClientProxy clientProxy;

    IUserService userService = null;
    @PostConstruct
    public void initProxy() {
        userService = (IUserService) clientProxy.createProxy(IUserService.class);
    }

    @GetMapping("user/{id}")
    public JSONObject getUser(@PathVariable("id")Integer id) {
        // catch NPE
        JSONObject result = new JSONObject();
        result.put("user", userService.getById(id));
        return result;
    }
}
